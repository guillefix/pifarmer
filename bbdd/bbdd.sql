DROP DATABASE pifarmer;

-- Script de creación de la BBDD
CREATE DATABASE IF NOT EXISTS pifarmer;
USE pifarmer;

CREATE TABLE IF NOT EXISTS User (
	username varchar(30) NOT NULL,
	password TEXT NOT NULL,
	email varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS Humitat(
	valor DECIMAL(4,1),
	data DATETIME
);

CREATE TABLE IF NOT EXISTS Temperatura(
	valor DECIMAL(4,1),
	data DATETIME
);

CREATE TABLE IF NOT EXISTS Nivell_aigua(
	valor DECIMAL(4,1),
	data DATETIME
);

CREATE TABLE IF NOT EXISTS Llum(
	valor BOOLEAN,
	data DATETIME
);

CREATE TABLE IF NOT EXISTS Alertas_email (
	id int primary key,
	nombre varchar(15),
	activar boolean,
	umbralMin DECIMAL(4,1),
	umbralMax DECIMAL(4,1)
);

CREATE TABLE IF NOT EXISTS Ventilacion (
	id int primary key,
	activar boolean,
	umbralTemp DECIMAL(4,1),
	duracionMin int
);

CREATE TABLE IF NOT EXISTS Dias_luces (
	id int primary key,
	activar boolean,
	diaSemana varchar(15),
	horaInicio TIME,
	horaFin TIME
);

CREATE TABLE IF NOT EXISTS Dias_riego (
	id int primary key,
	activar boolean,
	diaSemana varchar(15),
	horaActivar TIME
);

CREATE TABLE IF NOT EXISTS Deposito (
	id int primary key,
	distancia DECIMAL(4,1)
);

CREATE TABLE IF NOT EXISTS Alarmas_activadas (
	id int primary key,
	descripcio varchar(20),
	alarmaActivada boolean
);

CREATE TABLE IF NOT EXISTS `calendar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `startdate` varchar(48) NOT NULL,
  `enddate` varchar(48) NOT NULL,
  `allDay` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- Default user (admin:admin)
INSERT INTO User VALUES ("admin","21232f297a57a5a743894a0e4a801fc3","test@gmail.com");


-- Alertas_email
INSERT INTO Alertas_email VALUES (1, "Nivel agua", true, 25.0, null);
INSERT INTO Alertas_email VALUES (2, "Temperatura", true, 15.0, 25.0);
INSERT INTO Alertas_email VALUES (3, "Humedad", true, 75.0, 90.0);


-- Ventilacion
INSERT INTO Ventilacion VALUES (1, true, 30.0, 5);


-- Luces
INSERT INTO Dias_luces VALUES (1, true,"Lunes",'10:00','22:00');
INSERT INTO Dias_luces VALUES (2, true,"Martes",'10:00','22:00');
INSERT INTO Dias_luces VALUES (3, true,"Miercoles",'10:00','22:00');
INSERT INTO Dias_luces VALUES (4, true,"Jueves",'10:00','22:00');
INSERT INTO Dias_luces VALUES (5, true,"Viernes",'10:00','22:00');
INSERT INTO Dias_luces VALUES (6, true,"Sabado",'10:00','22:00');
INSERT INTO Dias_luces VALUES (7, true,"Domingo",'10:00','22:00');


-- Dias_riego
INSERT INTO Dias_riego VALUES (1, true ,"Lunes",'10:00');
INSERT INTO Dias_riego VALUES (2, false,"Martes",'10:00');
INSERT INTO Dias_riego VALUES (3, true ,"Miercoles",'10:00');
INSERT INTO Dias_riego VALUES (4, false,"Jueves",'10:00');
INSERT INTO Dias_riego VALUES (5, true ,"Viernes",'10:00');
INSERT INTO Dias_riego VALUES (6, false,"Sabado",'10:00');
INSERT INTO Dias_riego VALUES (7, false,"Domingo",'10:00');

-- Deposito

INSERT INTO Deposito VALUES (1, 15.0);

-- Alarmas_activadas
INSERT INTO Alarmas_activadas VALUES (1,"aguaMinima",false);
INSERT INTO Alarmas_activadas VALUES (2,"tempMinima",false);
INSERT INTO Alarmas_activadas VALUES (3,"tempMaxima",false);
INSERT INTO Alarmas_activadas VALUES (4,"humMinima" ,false);
INSERT INTO Alarmas_activadas VALUES (5,"humMaxima" ,false);
