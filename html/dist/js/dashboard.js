/*
 * 
 * This is a file used only for the main dashboard
 *
 */
  
	// pasarle dos parametros: id para el dato en concreto (0 = agua, 1 = temp, 2 = hum, 3 = luz) y el tiempo (24h, 1w [168h], 1m [720h])
	// Carga temperatura de temperatura.php (por defecto los últimos 24 registros/horas)
	var loadDada = function(id, temps) {
		var json;
		$.ajax({
			url: "gestores/gestorDades.php",
			data: { 'id':id, 'temps':temps },
			async: false,
			type: 'POST',
			success: function(data){
				obj = JSON.parse(data);
				json = obj;
			}
		});
		return json;
	}; 
	
	// Carga la última lectura de temperatura para mostrar en el icono correspondiente
	var ultimasLecturas = function() {
		var jsonAgua = loadDada(0,1);
		var jsonTemp = loadDada(1,1);
		var jsonHum = loadDada(2,1);
		var jsonLuz = loadDada(3,1);
		
		$("#ultimNivAgua").html(jsonAgua[0].item1 + '<sup style="font-size: 20px">%</sup>');
		$("#ultimaTemp").html(jsonTemp[0].item1 + '<sup style="font-size: 20px">ºC</sup>');
		$("#ultimaHum").html(jsonHum[0].item1 + '<sup style="font-size: 20px">%</sup>');
		
		var luzText = '';
		if (jsonLuz[0].item1 == "1") {
			luzText = "On";
			$("#iconoLuz").attr('class', 'ion ion-ios-sunny'); // si hay luz sale un sol
		}
		else if (jsonLuz[0].item1 == "0") {
			luzText = "Off";
			$("#iconoLuz").attr('class', 'ion ion-ios-moon'); // si no hay luz sale una luna
		}
		else {
			luzText = "-";
		}
		
		$("#ultimaLuz").html(luzText);
		
		$("#historial").html('<i class="fa fa-inbox"></i> Temperatura');
	}
	
	// Carga al inicio
	$(function () {
		ultimasLecturas();
		"use strict";
		
		// Cuando hagas clic en el "mostrar más" correspondiente, se asigna un id concreto 
		// Por defecto saldrá la temperatura al cargar la página
		var dada = 1;
		$(".small-box-footer").click(function(){
			var dadaId = $(this).attr('href').split('#');
			dada = parseInt(dadaId[1]);
			
			// Cambia el título de la gráfica
			if (dada == 0)
				$("#historial").html('<i class="fa fa-inbox"></i> Nivel de agua');
			if (dada == 1)
				$("#historial").html('<i class="fa fa-inbox"></i> Temperatura');
			if (dada == 2)
				$("#historial").html('<i class="fa fa-inbox"></i> Humedad');
			if (dada == 3)
				$("#historial").html('<i class="fa fa-inbox"></i> Luz');
			
			area.setData(loadDada(dada,24));
		});
		
		// Con esta función cambiamos los intervalos de tiempo de la gráfica al hacer clic en los tabs
		$('a[href="#revenue-chart"]').click(function(){
			area.setData(loadDada(dada,$(this).attr('id')));
		});	

		
	  //Make the dashboard widgets sortable Using jquery UI
	  $(".connectedSortable").sortable({
		placeholder: "sort-highlight",
		connectWith: ".connectedSortable",
		handle: ".box-header, .nav-tabs",
		forcePlaceholderSize: true,
		zIndex: 999999
	  });
	  $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");

	  //jQuery UI sortable for the todo list
	  $(".todo-list").sortable({
		placeholder: "sort-highlight",
		handle: ".handle",
		forcePlaceholderSize: true,
		zIndex: 999999
	  });

	  $('.daterange').daterangepicker({
		ranges: {
		  'Today': [moment(), moment()],
		  'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		  'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		  'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		  'This Month': [moment().startOf('month'), moment().endOf('month')],
		  'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		},
		startDate: moment().subtract(29, 'days'),
		endDate: moment()
	  }, function (start, end) {
		window.alert("You chose: " + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
	  });
	  
	  //The Calender
	  $("#calendar").datepicker();

	  /* Morris.js Chart */
	  // Gráfico temperatura
	  var area = new Morris.Area({
		element: 'revenue-chart',
		resize: true,
		data: loadDada(1,24),
		xkey: 'y',
		ykeys: ['item1'],
		labels: ['Valor'],
		lineColors: ['#a0d0e0'],
		hideHover: 'auto'
	  });

	  //Fix for charts under tabs
	  $('.box ul.nav a').on('shown.bs.tab', function () {
		area.redraw();
	  });

	  /* The todo list plugin */
	  $(".todo-list").todolist({
		onCheck: function (ele) {
		  window.console.log("The element has been checked");
		  return ele;
		},
		onUncheck: function (ele) {
		  window.console.log("The element has been unchecked");
		  return ele;
		}
	  });

	});
