
	// Carga los datos de la BBDD. El parámetro será la url del gestor de datos. 
	var loadDataDB = function(urlDatos) {
		var json;
		$.ajax({
			url: "gestores/" + urlDatos,
			async: false,
			type: 'POST',
			success: function(data){
				obj = JSON.parse(data);
				json = obj;
			}
		});
		return json;
	}; 
	
	// Pone los valores por defecto de las alertas 
	var alertasDefault = function() {
		
		// Checks
		$('input[name="activarAgua"]').attr("checked", true);
		$('input[name="activarTemp"]').attr("checked", true);
		$('input[name="activarHum"]').attr("checked", true);
		
		// Umbrales mínimos
		$('input[name="umbralAgua"]').val(25);
		$('input[name="umbralTempMin"]').val(15);
		$('input[name="umbralHumMin"]').val(75);
		
		// Umbrales máximos
		$('input[name="umbralTempMax"]').val(25);
		$('input[name="umbralHumMax"]').val(90);
	};
	
	// Pone los valores por defecto de los actuadores
	var actuadoresDefault = function() {
		
		// Ventiladores
		$('input[name="activarVentiladores"]').attr("checked",true);
		$('input[name="umbralTempVent"]').val(30);
		$('input[name="minutos"]').val(5);
		
		
		// Luces
		// Checks
		$('input[name="activarLucesLunes"]').attr("checked", true);
		$('input[name="activarLucesMartes"]').attr("checked", true);
		$('input[name="activarLucesMiercoles"]').attr("checked", true);
		$('input[name="activarLucesJueves"]').attr("checked", true);
		$('input[name="activarLucesViernes"]').attr("checked", true);
		$('input[name="activarLucesSabado"]').attr("checked", true);
		$('input[name="activarLucesDomingo"]').attr("checked", true);
		// Horas iniciales
		$('select[name="lunesInicio"]').val("10:00");
		$('select[name="martesInicio"]').val("10:00");
		$('select[name="miercolesInicio"]').val("10:00");
		$('select[name="juevesInicio"]').val("10:00");
		$('select[name="viernesInicio"]').val("10:00");
		$('select[name="sabadoInicio"]').val("10:00");
		$('select[name="domingoInicio"]').val("10:00");
		// Horas finales
		$('select[name="lunesFinal"]').val("22:00");
		$('select[name="martesFinal"]').val("22:00");
		$('select[name="miercolesFinal"]').val("22:00");
		$('select[name="juevesFinal"]').val("22:00");
		$('select[name="viernesFinal"]').val("22:00");
		$('select[name="sabadoFinal"]').val("22:00");
		$('select[name="domingoFinal"]').val("22:00");
		
		
		// Riego
		// Checks
		$('input[name="activarRiegoLunes"]').attr("checked", true);
		$('input[name="activarRiegoMartes"]').attr("checked", false);
		$('input[name="activarRiegoMiercoles"]').attr("checked", true);
		$('input[name="activarRiegoJueves"]').attr("checked", false);
		$('input[name="activarRiegoViernes"]').attr("checked", true);
		$('input[name="activarRiegoSabado"]').attr("checked", false);
		$('input[name="activarRiegoDomingo"]').attr("checked", false);
		// Hora activar
		$('select[name="lunesActiva"]').val("10:00");
		$('select[name="martesActiva"]').val("10:00");
		$('select[name="miercolesActiva"]').val("10:00");
		$('select[name="juevesActiva"]').val("10:00");
		$('select[name="viernesActiva"]').val("10:00");
		$('select[name="sabadoActiva"]').val("10:00");
		$('select[name="domingoActiva"]').val("10:00");
		
		// Deposito
		$('input[name="distanciaDep"]').val(15);
	};
	
	// Carga al inicio
	$(function () {
		
		/* ALERTAS */
		// Mostrar en los settings de alertas los datos que hay en la BBDD
		var dataAlertas = loadDataDB("recibirAlertas.php");

		// Comprobar si los checked están activos o no
		if(dataAlertas[0].activar == 1)
			$('input[name="activarAgua"]').attr("checked", true);
		if(dataAlertas[1].activar == 1)
			$('input[name="activarTemp"]').attr("checked", true);
		if(dataAlertas[2].activar == 1)
			$('input[name="activarHum"]').attr("checked", true);
		
		// Umbrales mímimos
		$('input[name="umbralAgua"]').val(dataAlertas[0].umbralMin);
		$('input[name="umbralTempMin"]').val(dataAlertas[1].umbralMin);
		$('input[name="umbralHumMin"]').val(dataAlertas[2].umbralMin);
		
		// Umbrales máximos
		$('input[name="umbralTempMax"]').val(dataAlertas[1].umbralMax);
		$('input[name="umbralHumMax"]').val(dataAlertas[2].umbralMax);
		
		
		/* USER */
		var dataUser = loadDataDB("recibirUser.php");

		$('input[name="email"]').val(dataUser[0].email);
		
		
		/* ACTUADORES */
			/* Ventilacion */
			var dataActuadoresVentilacion = loadDataDB("recibirActuadoresVentilacion.php");
			if (dataActuadoresVentilacion[0].activar == 1)
				$('input[name="activarVentiladores"]').attr("checked", true);
			$('input[name="umbralTempVent"]').val(dataActuadoresVentilacion[0].umbralTemp);
			$('input[name="duracionMin"]').val(dataActuadoresVentilacion[0].duracionMin);
			
			/* Luces */
			var dataActuadoresLuces = loadDataDB("recibirActuadoresLuces.php");
			// Comprobar si los checked están activos o no
			if(dataActuadoresLuces[0].activar == 1)
				$('input[name="activarLucesLunes"]').attr("checked", true);
			if(dataActuadoresLuces[1].activar == 1)
				$('input[name="activarLucesMartes"]').attr("checked", true);
			if(dataActuadoresLuces[2].activar == 1)
				$('input[name="activarLucesMiercoles"]').attr("checked", true);
			if(dataActuadoresLuces[3].activar == 1)
				$('input[name="activarLucesJueves"]').attr("checked", true);
			if(dataActuadoresLuces[4].activar == 1)
				$('input[name="activarLucesViernes"]').attr("checked", true);
			if(dataActuadoresLuces[5].activar == 1)
				$('input[name="activarLucesSabado"]').attr("checked", true);
			if(dataActuadoresLuces[6].activar == 1)
				$('input[name="activarLucesDomingo"]').attr("checked", true);
			// Hora inicio
			$('select[name="lunesInicio"]').val(dataActuadoresLuces[0].horaInicio);
			$('select[name="martesInicio"]').val(dataActuadoresLuces[1].horaInicio);
			$('select[name="miercolesInicio"]').val(dataActuadoresLuces[2].horaInicio);
			$('select[name="juevesInicio"]').val(dataActuadoresLuces[3].horaInicio);
			$('select[name="viernesInicio"]').val(dataActuadoresLuces[4].horaInicio);
			$('select[name="sabadoInicio"]').val(dataActuadoresLuces[5].horaInicio);
			$('select[name="domingoInicio"]').val(dataActuadoresLuces[6].horaInicio);
			// Hora final
			$('select[name="lunesFinal"]').val(dataActuadoresLuces[0].horaFin);
			$('select[name="martesFinal"]').val(dataActuadoresLuces[1].horaFin);
			$('select[name="miercolesFinal"]').val(dataActuadoresLuces[2].horaFin);
			$('select[name="juevesFinal"]').val(dataActuadoresLuces[3].horaFin);
			$('select[name="viernesFinal"]').val(dataActuadoresLuces[4].horaFin);
			$('select[name="sabadoFinal"]').val(dataActuadoresLuces[5].horaFin);
			$('select[name="domingoFinal"]').val(dataActuadoresLuces[6].horaFin);
			
			/* Riego */
			var dataActuadoresRiego = loadDataDB("recibirActuadoresRiego.php");
			// Comprobar si los checked están activos o no
			if(dataActuadoresRiego[0].activar == 1)
				$('input[name="activarRiegoLunes"]').attr("checked", true);
			if(dataActuadoresRiego[1].activar == 1)
				$('input[name="activarRiegoMartes"]').attr("checked", true);
			if(dataActuadoresRiego[2].activar == 1)
				$('input[name="activarRiegoMiercoles"]').attr("checked", true);
			if(dataActuadoresRiego[3].activar == 1)
				$('input[name="activarRiegoJueves"]').attr("checked", true);
			if(dataActuadoresRiego[4].activar == 1)
				$('input[name="activarRiegoViernes"]').attr("checked", true);
			if(dataActuadoresRiego[5].activar == 1)
				$('input[name="activarRiegoSabado"]').attr("checked", true);
			if(dataActuadoresRiego[6].activar == 1)
				$('input[name="activarRiegoDomingo"]').attr("checked", true);
			// Hora activar
			$('select[name="lunesActiva"]').val(dataActuadoresRiego[0].horaActivar);
			$('select[name="martesActiva"]').val(dataActuadoresRiego[1].horaActivar);
			$('select[name="miercolesActiva"]').val(dataActuadoresRiego[2].horaActivar);
			$('select[name="juevesActiva"]').val(dataActuadoresRiego[3].horaActivar);
			$('select[name="viernesActiva"]').val(dataActuadoresRiego[4].horaActivar);
			$('select[name="sabadoActiva"]').val(dataActuadoresRiego[5].horaActivar);
			$('select[name="domingoActiva"]').val(dataActuadoresRiego[6].horaActivar);	
			
			/* Depósito */
			var dataActuadoresDeposito = loadDataDB("recibirActuadoresDeposito.php");
			$('input[name="distanciaDep"]').val(dataActuadoresDeposito[0].distancia);
	});
