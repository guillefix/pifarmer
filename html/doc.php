<?php 
	include_once 'redirect.php';
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PiFarmer | Documentación</title>
  <link rel="icon" type="image/x-icon" href="dist/img/favicon.ico" />
  
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>Pi</b>F</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Pi</b>Farmer</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="dist/img/user.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $_SESSION['user']; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="dist/img/user.jpg" class="img-circle" alt="User Image">

                <p>
                  <?php echo $_SESSION['user']; ?>
                  <small>Cultivador profesional</small>
                </p>
              </li>
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="settings.php" class="btn btn-default btn-flat">Perfil</a>
                </div>
                <div class="pull-right">
                  <a href="logout.php" class="btn btn-default btn-flat">Logout</a>
                </div>
              </li>
            </ul>
          </li
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">NAVEGACIÓN PRINCIPAL</li>
		<li><a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
		<li><a href="settings.php"><i class="fa fa-gears "></i> <span>Ajustes</span></a></li>
		<li><a href="calendar.php"><i class="fa fa-calendar "></i> <span>Calendario</span></a></li>
        <li><a href="camera.php"><i class="fa fa-video-camera "></i> <span>Cámara</span></a></li>
        <li class="active"><a href="doc.php"><i class="fa fa-book"></i> <span>Documentación</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Documentación
        <small>Información de uso</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Documentación</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-md-6">
			<div class="box box-solid box-primary">
				<div class="box-header with-border">
				  <h3 class="box-title">Dashboard</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<p>En el Dashboard o Panel de Control podrás hacer un seguimiento de lo siguiente:</p>
						<ul>
							<li>Nivel del depósito de <b>agua</b></li>
							<li><b>Temperatura</b> del aire</li>
							<li><b>Humedad</b> del aire</li>
							<li>Si las <b>luces</b> están encendidas o apagadas</li>
						</ul>
						<p>En la parte superior podemos encontrar 4 cajas, cada una mostrando la última lectura realizada y la variable en cuestión.
						</p>
						<p>En la parte inferior podemos ver dos secciones. La primera es una gráfica con el histórico de valores (puedes elegir cuantas horas/registros mostrar). La segunda es una lista
						de tareas (To-do list) personal.</p>
						<p>Haciendo click en la opción <code>Mostrar más</code> de cada caja, la gráfica cambiará en función de lo que hayas escogido.</p>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
			
			<div class="box box-solid box-primary">
				<div class="box-header with-border">
				  <h3 class="box-title">Ajustes</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<p>En Ajustes puedes configurar las alertas, tu información personal y los actuadores.</p>
						<h4><b>Alertas:</b></h4>
						<p>Aquí puedes habilitar las alertas que se enviarán por <b>e-mail</b> cuando los valores en cuestión estén por encima o por debajo de los <b>umbrales</b> deseados por ti.</p>
						<p>Por ejemplo: si deseas recibir un e-mail cada vez que la temperatura baje de 10ºC o supere los 30ºC, debes modificar los valores o umbrales mínimos y máximos.</p>
						<p>Otro ejemplo: si no deseas recibir alertas sobre el nivel de agua del depósito pero sí de la temperatura y la humedad, debes desmarcar la primera opción y marcar las otras dos.</p>
						<h4><b>Información personal:</b></h4>
						<p>Aquí puedes modificar la siguiente información del usuario:</p>
						<ul>
							<li>Nombre de usuario</li>
							<li>E-mail (usado para recibir alertas)</li>
							<li>Contraseña</li>
						</ul>
						<h4><b>Actuadores:</b></h4>
						<p>Aquí puedes cambiar el comportamiento de los actuadores (placa de relés).</p>
						<ul>
							<li>Ventilación: se activará durante X minutos si la temperatura sobrepasa el umbral definido.</li>
							<li>Luces: se activarán durante los días y horas que elijas. P. ej., todos los días de la semana de 10:00 a 22:00.</li>
							<li>Riego: se programan los días de la semana y hora a la que el riego automático saltará.</li>
							<li>Centímetros del depósito de agua: hay que medir manualmente la distancia que hay entre el fondo del depósito y el sensor de distancia para 
							poder calcular correctamente el porcentaje de agua disponible.</li>
						</ul>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-md-6">
			<div class="box box-solid box-primary">
				<div class="box-header with-border">
				  <h3 class="box-title">Calendario</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<p>Aquí puedes añadir <b>eventos</b> en el calendario.</p>
						<p>Por ejemplo, si el próximo lunes tienes que cambiar las horas de luz, puedes añadir un evento
						con esa información para que te acuerdes.</p>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->          
			<div class="box box-solid box-primary">
				<div class="box-header with-border">
				  <h3 class="box-title">Cámara</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<p>Esta sección se divide en dos partes:</p>
						<ul>
							<li><b>Cámara en directo:</b> puedes hacer un seguimiento en vivo de lo que está sucediendo. Hay que tener en cuenta que hay un pequeño desfase de un par de segundos.</li>
							<li><b>Historial diario de fotos:</b> puedes hacer un seguimiento del progreso que has ido obteniendo de tu plantación con fotos tomadas a diario.</li>
						</ul>
						<p>Cuando se toma la foto diaria el proceso que permite ver la imagen en directo se detiene durante unos segundos para no entrar en conflicto.</p>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->          
			<div class="box box-solid box-primary">
				<div class="box-header with-border">
				  <h3 class="box-title">Repositorio</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<p>Enlace al repositorio git: <a href="https://bitbucket.org/guillermo_a14_figueras_alba/pifarmer.git">https://bitbucket.org/guillermo_a14_figueras_alba/pifarmer.git</a></p>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->          
        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>AdminLTE Version</b> 2.3.3
    </div>
    <strong>Creado por Guillermo Figueras</a>.</strong> 
  </footer>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
<script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
