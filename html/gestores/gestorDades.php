<?php
	include '../db_conn.php';

	if (isset($_POST['id']))
		$id = $_POST['id'];
	else 
		$id = '';
		
	filter_var($id, FILTER_SANITIZE_NUMBER_INT);

	// Selecciona la tabla de la BBDD
	switch($id) {
		case 0:
			$id = 'Nivell_aigua';
			break;
		case 1:
			$id = 'Temperatura';
			break;
		case 2:
			$id = 'Humitat';
			break;
		case 3:
			$id = 'Llum';
			break;
	}

	$temps = $_POST['temps'];
	filter_var($temps, FILTER_SANITIZE_NUMBER_FLOAT);

	if($resultat = $conn->query('SELECT * FROM '.$id. ' ORDER BY data DESC LIMIT '.$temps)){
		while($row=mysqli_fetch_array($resultat,MYSQLI_NUM)){
			$data[] = array(
				'y' => $row[1], 
				'item1' => $row[0], 
			);
		}
	}

	$conn->close();

	echo json_encode($data);
?> 
