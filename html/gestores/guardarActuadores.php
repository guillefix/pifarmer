<?php
	include '../db_conn.php';
	
	function errorRedirect() {
		header( 'Location: ../settings.php?hecho=0' );
		die();
	}
	
	// SANEAR VARIABLES
	// Ventiladores
	$activarVentiladores = filter_var($_POST['activarVentiladores'], FILTER_SANITIZE_STRING);
	
	$activarVentiladores = ($activarVentiladores == "on") ? 1 : 0;
	$umbralTempVent = filter_var($_POST['umbralTempVent'], FILTER_SANITIZE_NUMBER_INT);
	$duracionMin = filter_var($_POST['duracionMin'], FILTER_SANITIZE_NUMBER_INT);
	// Dias_luces
		// Checks
		$activarLucesLunes = filter_var($_POST['activarLucesLunes'], FILTER_SANITIZE_STRING);
		$activarLucesMartes = filter_var($_POST['activarLucesMartes'], FILTER_SANITIZE_STRING);
		$activarLucesMiercoles = filter_var($_POST['activarLucesMiercoles'], FILTER_SANITIZE_STRING);
		$activarLucesJueves = filter_var($_POST['activarLucesJueves'], FILTER_SANITIZE_STRING);
		$activarLucesViernes = filter_var($_POST['activarLucesViernes'], FILTER_SANITIZE_STRING);
		$activarLucesSabado = filter_var($_POST['activarLucesSabado'], FILTER_SANITIZE_STRING);
		$activarLucesDomingo = filter_var($_POST['activarLucesDomingo'], FILTER_SANITIZE_STRING);
		
		$activarLucesLunes = ($activarLucesLunes == "on") ? 1 : 0;
		$activarLucesMartes = ($activarLucesMartes == "on") ? 1 : 0;
		$activarLucesMiercoles = ($activarLucesMiercoles == "on") ? 1 : 0;
		$activarLucesJueves = ($activarLucesJueves == "on") ? 1 : 0;
		$activarLucesViernes = ($activarLucesViernes == "on") ? 1 : 0;
		$activarLucesSabado = ($activarLucesSabado == "on") ? 1 : 0;
		$activarLucesDomingo = ($activarLucesDomingo == "on") ? 1 : 0;
		// Hora inicio
		$lunesInicio = filter_var($_POST['lunesInicio'], FILTER_SANITIZE_STRING);
		$martesInicio = filter_var($_POST['martesInicio'], FILTER_SANITIZE_STRING);
		$miercolesInicio = filter_var($_POST['miercolesInicio'], FILTER_SANITIZE_STRING);
		$juevesInicio = filter_var($_POST['juevesInicio'], FILTER_SANITIZE_STRING);
		$viernesInicio = filter_var($_POST['viernesInicio'], FILTER_SANITIZE_STRING);
		$sabadoInicio = filter_var($_POST['sabadoInicio'], FILTER_SANITIZE_STRING);
		$domingoInicio = filter_var($_POST['domingoInicio'], FILTER_SANITIZE_STRING);
		// Hora final
		$lunesFinal = filter_var($_POST['lunesFinal'], FILTER_SANITIZE_STRING);
		$martesFinal = filter_var($_POST['martesFinal'], FILTER_SANITIZE_STRING);
		$miercolesFinal = filter_var($_POST['miercolesFinal'], FILTER_SANITIZE_STRING);
		$juevesFinal = filter_var($_POST['juevesFinal'], FILTER_SANITIZE_STRING);
		$viernesFinal = filter_var($_POST['viernesFinal'], FILTER_SANITIZE_STRING);
		$sabadoFinal = filter_var($_POST['sabadoFinal'], FILTER_SANITIZE_STRING);
		$domingoFinal = filter_var($_POST['domingoFinal'], FILTER_SANITIZE_STRING);
	// Dias_riego
		// Checks
		$activarRiegoLunes = filter_var($_POST['activarRiegoLunes'], FILTER_SANITIZE_STRING);
		$activarRiegoMartes = filter_var($_POST['activarRiegoMartes'], FILTER_SANITIZE_STRING);
		$activarRiegoMiercoles = filter_var($_POST['activarRiegoMiercoles'], FILTER_SANITIZE_STRING);
		$activarRiegoJueves = filter_var($_POST['activarRiegoJueves'], FILTER_SANITIZE_STRING);
		$activarRiegoViernes = filter_var($_POST['activarRiegoViernes'], FILTER_SANITIZE_STRING);
		$activarRiegoSabado = filter_var($_POST['activarRiegoSabado'], FILTER_SANITIZE_STRING);
		$activarRiegoDomingo = filter_var($_POST['activarRiegoDomingo'], FILTER_SANITIZE_STRING);
		
		$activarRiegoLunes = ($activarRiegoLunes == "on") ? 1 : 0;
		$activarRiegoMartes = ($activarRiegoMartes == "on") ? 1 : 0;
		$activarRiegoMiercoles = ($activarRiegoMiercoles == "on") ? 1 : 0;
		$activarRiegoJueves = ($activarRiegoJueves == "on") ? 1 : 0;
		$activarRiegoViernes = ($activarRiegoViernes == "on") ? 1 : 0;
		$activarRiegoSabado = ($activarRiegoSabado == "on") ? 1 : 0;
		$activarRiegoDomingo = ($activarRiegoDomingo == "on") ? 1 : 0;
		// Hora activar
		$lunesActiva = filter_var($_POST['lunesActiva'], FILTER_SANITIZE_STRING);
		$martesActiva = filter_var($_POST['martesActiva'], FILTER_SANITIZE_STRING);
		$miercolesActiva = filter_var($_POST['miercolesActiva'], FILTER_SANITIZE_STRING);
		$juevesActiva = filter_var($_POST['juevesActiva'], FILTER_SANITIZE_STRING);
		$viernesActiva = filter_var($_POST['viernesActiva'], FILTER_SANITIZE_STRING);
		$sabadoActiva = filter_var($_POST['sabadoActiva'], FILTER_SANITIZE_STRING);
		$domingoActiva = filter_var($_POST['domingoActiva'], FILTER_SANITIZE_STRING);
	// Deposito
	$distanciaDep = filter_var($_POST['distanciaDep'], FILTER_SANITIZE_NUMBER_INT);
	
	// Comprobar que las horas de luz de inicio no sean superiores a las de finalizacion
	if ($lunesInicio >= $lunesFinal) 
		errorRedirect();
	if ($martesInicio >= $martesFinal) 
		errorRedirect();
	if ($miercolesInicio >= $miercolesFinal) 
		errorRedirect();
	if ($juevesInicio >= $juevesFinal) 
		errorRedirect();
	if ($viernesInicio >= $viernesFinal) 
		errorRedirect();
	if ($sabadoInicio >= $sabadoFinal) 
		errorRedirect();
	if ($domingoInicio >= $domingoFinal) 
		errorRedirect();
	
	// Queries Update
	
	$updateVentilacion="UPDATE Ventilacion SET activar=".$activarVentiladores.", umbralTemp=".$umbralTempVent." , duracionMin=".$duracionMin." WHERE id=1";
	
	$updateLucesLunes="UPDATE Dias_luces SET activar=".$activarLucesLunes.", horaInicio='".$lunesInicio."', horaFin='".$lunesFinal."' WHERE id=1";
	$updateLucesMartes="UPDATE Dias_luces SET activar=".$activarLucesMartes.", horaInicio='".$martesInicio."', horaFin='".$martesFinal."' WHERE id=2";
	$updateLucesMiercoles="UPDATE Dias_luces SET activar=".$activarLucesMiercoles.", horaInicio='".$miercolesInicio."', horaFin='".$miercolesFinal."' WHERE id=3";
	$updateLucesJueves="UPDATE Dias_luces SET activar=".$activarLucesJueves.", horaInicio='".$juevesInicio."', horaFin='".$juevesFinal."' WHERE id=4";
	$updateLucesViernes="UPDATE Dias_luces SET activar=".$activarLucesViernes.", horaInicio='".$viernesInicio."', horaFin='".$viernesFinal."' WHERE id=5";
	$updateLucesSabado="UPDATE Dias_luces SET activar=".$activarLucesSabado.", horaInicio='".$sabadoInicio."', horaFin='".$sabadoFinal."' WHERE id=6";
	$updateLucesDomingo="UPDATE Dias_luces SET activar=".$activarLucesDomingo.", horaInicio='".$domingoInicio."', horaFin='".$domingoFinal."' WHERE id=7";
	
	$updateRiegoLunes="UPDATE Dias_riego SET activar=".$activarRiegoLunes.", horaActivar='".$lunesActiva."' WHERE id=1";
	$updateRiegoMartes="UPDATE Dias_riego SET activar=".$activarRiegoMartes.", horaActivar='".$martesActiva."' WHERE id=2";
	$updateRiegoMiercoles="UPDATE Dias_riego SET activar=".$activarRiegoMiercoles.", horaActivar='".$miercolesActiva."' WHERE id=3";
	$updateRiegoJueves="UPDATE Dias_riego SET activar=".$activarRiegoJueves.", horaActivar='".$juevesActiva."' WHERE id=4";
	$updateRiegoViernes="UPDATE Dias_riego SET activar=".$activarRiegoViernes.", horaActivar='".$viernesActiva."' WHERE id=5";
	$updateRiegoSabado="UPDATE Dias_riego SET activar=".$activarRiegoSabado.", horaActivar='".$sabadoActiva."' WHERE id=6";
	$updateRiegoDomingo="UPDATE Dias_riego SET activar=".$activarRiegoDomingo.", horaActivar='".$domingoActiva."' WHERE id=7";
	
	$updateDeposito="UPDATE Deposito SET distancia=".$distanciaDep." WHERE id=1";
	
	// Si algún update no se hace correctamente muestra un mensaje de error en settings.php
	if ($conn->query($updateVentilacion) === FALSE) 
		errorRedirect();
	
	if ($conn->query($updateLucesLunes) === FALSE) 
		errorRedirect();
	if ($conn->query($updateLucesMartes) === FALSE) 
		errorRedirect();
	if ($conn->query($updateLucesMiercoles) === FALSE) 
		errorRedirect();
	if ($conn->query($updateLucesJueves) === FALSE) 
		errorRedirect();
	if ($conn->query($updateLucesViernes) === FALSE) 
		errorRedirect();
	if ($conn->query($updateLucesSabado) === FALSE) 
		errorRedirect();
	if ($conn->query($updateLucesDomingo) === FALSE) 
		errorRedirect();
		
	if ($conn->query($updateRiegoLunes) === FALSE) 
		errorRedirect();
	if ($conn->query($updateRiegoMartes) === FALSE) 
		errorRedirect();
	if ($conn->query($updateRiegoMiercoles) === FALSE) 
		errorRedirect();
	if ($conn->query($updateRiegoJueves) === FALSE) 
		errorRedirect();
	if ($conn->query($updateRiegoViernes) === FALSE) 
		errorRedirect();
	if ($conn->query($updateRiegoSabado) === FALSE) 
		errorRedirect();
	if ($conn->query($updateRiegoDomingo) === FALSE) 
		errorRedirect();
		
	if ($conn->query($updateDeposito) === FALSE) 
		errorRedirect();
	
	$conn->close();
	
	// Si todos los updates se han hecho correctamente muestra un mensaje de OK
	header( 'Location: ../settings.php?hecho=1' );
?>
