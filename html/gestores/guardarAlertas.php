<?php
	include '../db_conn.php';
	
	function errorRedirect() {
		header( 'Location: ../settings.php?hecho=0' );
		die();
	}
	
	// Sanear variables
	filter_var($_POST['activarAgua'], FILTER_SANITIZE_STRING);
	filter_var($_POST['activarTemp'], FILTER_SANITIZE_STRING);
	filter_var($_POST['activarHum'] , FILTER_SANITIZE_STRING);
	
	filter_var($_POST['umbralAgua']   , FILTER_SANITIZE_NUMBER_FLOAT);
	filter_var($_POST['umbralTempMin'], FILTER_SANITIZE_NUMBER_FLOAT);
	filter_var($_POST['umbralHumMin'] , FILTER_SANITIZE_NUMBER_FLOAT);

	filter_var($_POST['umbralTempMax'], FILTER_SANITIZE_NUMBER_FLOAT);
	filter_var($_POST['umbralHumMax'] , FILTER_SANITIZE_NUMBER_FLOAT);
	
	// Comprobar si se ha checkeado el activar
	if($_POST['activarAgua'] == "on")
		$activarAgua = 1;
	else
		$activarAgua = 0;
		
	if($_POST['activarTemp'] == "on")
		$activarTemp = 1;
	else
		$activarTemp = 0;
		
	if($_POST['activarHum'] == "on")
		$activarHum = 1;
	else
		$activarHum = 0;
	
	// IMPLEMENTAR CONDICION MAYOR O MENOR UMBRALES
	
	$umbralAgua = $_POST['umbralAgua'];
	
	$umbralTempMin = $_POST['umbralTempMin'];
	$umbralTempMax = $_POST['umbralTempMax'];
	
	$umbralHumMin = $_POST['umbralHumMin'];
	$umbralHumMax = $_POST['umbralHumMax'];
	
	if ($umbralTempMin >= $umbralTempMax) 
		errorRedirect();
	if ($umbralHumMin >= $umbralHumMax) 
		errorRedirect();
	
	$updateAgua = "UPDATE Alertas_email SET activar=".$activarAgua.", umbralMin=".$umbralAgua."   , umbralMax=null WHERE id=1";
	$updateTemp = "UPDATE Alertas_email SET activar=".$activarTemp.", umbralMin=".$umbralTempMin.", umbralMax=".$umbralTempMax." WHERE id=2";
	$updateHum  = "UPDATE Alertas_email SET activar=".$activarHum .", umbralMin=".$umbralHumMin." , umbralMax=".$umbralHumMax. " WHERE id=3";
	
	// Si algún update no se hace correctamente muestra un mensaje de error en settings.php
	if ($conn->query($updateAgua) === FALSE) 
		errorRedirect();
	if ($conn->query($updateTemp) === FALSE) 
		errorRedirect();
	if ($conn->query($updateHum)  === FALSE) 
		errorRedirect();
	

	$conn->close();
	
	// Si todos los updates se han hecho correctamente muestra un mensaje de OK
	header( 'Location: ../settings.php?hecho=1' );
?>
