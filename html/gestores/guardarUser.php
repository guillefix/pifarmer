<?php
	session_start();

	include '../db_conn.php';
	
	function errorRedirect() {
		header( 'Location: ../settings.php?hecho=0' );
		die();
	}
	
	// Sanear variables
	filter_var($_POST['username'], FILTER_SANITIZE_STRING);
	filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
	filter_var($_POST['passActual'], FILTER_SANITIZE_STRING);
	filter_var($_POST['passNueva'], FILTER_SANITIZE_STRING);
	filter_var($_POST['passNueva2'], FILTER_SANITIZE_STRING);
	
	$username = $_POST['username'];
	$email = $_POST['email'];
	$passNueva = md5($_POST['passNueva']);
	$passActual = md5($_POST['passActual']);
	$userSession = $_SESSION['user'];

	// Comprobar que la contraseña actual sea correcta
	$checkPassActual = $conn->query("SELECT * FROM User WHERE username='".$userSession."' AND password='".$passActual."'");
	if (mysqli_num_rows($checkPassActual) > 0) {
		$_SESSION['user'] = $_POST['username'];
		$checkPassActual->close();
		
		// Comprobar que la contraseña nueva está bien reescrita
		if ($_POST['passNueva'] == $_POST['passNueva2']) {
			
			$updateUser = "UPDATE User SET username='".$username."', password='".$passNueva."', email='".$email."' WHERE username='".$userSession."'";
			// Ejecutar query
			if ($conn->query($updateUser) === FALSE) {
				errorRedirect();
			}
		}
		else{
			errorRedirect();
		}
	}
	else{
		errorRedirect();
	}

	$conn->close();
	
	// Si todos los updates se han hecho correctamente muestra un mensaje de OK
	header( 'Location: ../settings.php?hecho=1' );
?>
