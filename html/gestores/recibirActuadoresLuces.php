<?php
	include '../db_conn.php';
	
	if($resultat = $conn->query('SELECT * FROM Dias_luces')){
		while($row = mysqli_fetch_array($resultat, MYSQLI_NUM)){
			$data[] = array(
				'activar' => $row[1],
				'horaInicio' => substr($row[3], 0, 5),				
				'horaFin' => substr($row[4], 0, 5),
			);
		}
	}
	
	$conn->close();

	echo json_encode($data);
?>
