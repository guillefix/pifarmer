<?php
	include '../db_conn.php';
	
	if($resultat = $conn->query('SELECT * FROM Dias_riego')){
		while($row = mysqli_fetch_array($resultat, MYSQLI_NUM)){
			$data[] = array(
				'activar' => $row[1],
				'horaActivar' => substr($row[3], 0, 5),
			);
		}
	}
	
	$conn->close();

	echo json_encode($data);
?>
