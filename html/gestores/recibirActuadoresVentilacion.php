<?php
	include '../db_conn.php';
	
	if($resultat = $conn->query('SELECT * FROM Ventilacion')){
		while($row = mysqli_fetch_array($resultat, MYSQLI_NUM)){
			$data[] = array(
				'activar' => $row[1], 
				'umbralTemp' => (int) $row[2], 
				'duracionMin' => $row[3],
			);
		}
	}
	
	$conn->close();

	echo json_encode($data);
?>
