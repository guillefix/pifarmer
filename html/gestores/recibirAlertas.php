<?php
	include '../db_conn.php';
	
	if($resultat = $conn->query('SELECT * FROM Alertas_email')){
		while($row = mysqli_fetch_array($resultat, MYSQLI_NUM)){
			$data[] = array(
				'activar' => $row[2], 
				'umbralMin' => (int) $row[3], 
				'umbralMax' => (int) $row[4],				
			);
		}
	}	
	
	$conn->close();

	echo json_encode($data);
?>
