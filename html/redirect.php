<?php
	// redirigir al usuario al login en el caso de que no haya iniciado sesión e intente acceder a sitios sin autorización
	// incluirlo en todas las páginas
	session_start();
	if(!isset($_SESSION['user'])) {
		header('Location: login.php');
	}
?>
