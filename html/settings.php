<?php 
	include_once 'redirect.php';
	
	function generaHoras($dia) {
		echo "<select name='".$dia."'>";
		for ($i = 0; $i < 24; $i++) {
			if ($i < 10)
				echo "<option value='0$i:00'>0$i:00</option>";
			else
				echo "<option value='$i:00'>$i:00</option>";
		}
		echo "</select>";
	}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PiFarmer | Ajustes</title>
  <link rel="icon" type="image/x-icon" href="dist/img/favicon.ico" />
  
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <!-- sweetalert css -->
  <link rel="stylesheet" href="plugins/sweetalert/sweetalert.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>Pi</b>F</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Pi</b>Farmer</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="dist/img/user.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $_SESSION['user']; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="dist/img/user.jpg" class="img-circle" alt="User Image">

                <p>
                  <?php echo $_SESSION['user']; ?>
                  <small>Cultivador profesional</small>
                </p>
              </li>
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="settings.php" class="btn btn-default btn-flat">Perfil</a>
                </div>
                <div class="pull-right">
                  <a href="logout.php" class="btn btn-default btn-flat">Logout</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">NAVEGACIÓN PRINCIPAL</li>
		<li><a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
		<li class="active"><a href="settings.php"><i class="fa fa-gears "></i> <span>Ajustes</span></a></li>
		<li><a href="calendar.php"><i class="fa fa-calendar "></i> <span>Calendario</span></a></li>
        <li><a href="camera.php"><i class="fa fa-video-camera "></i> <span>Cámara</span></a></li>
        <li><a href="doc.php"><i class="fa fa-book"></i> <span>Documentación</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Ajustes
        <small>Configuración general</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Ajustes</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-md-6">
			<div class="box box-solid box-primary">
				<div class="box-header with-border">
				  <h3 class="box-title">Alertas</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<form action="gestores/guardarAlertas.php" method="POST">
							<!--Si el umbral mínimo es superior al máximo, no guarda nada-->
							Recibirás una alerta por e-mail cuando los valores a continuación estén por debajo o por encima del umbral deseado.
							<br>
							<br>
							<table class="table table-bordered table-striped dataTable">
								<tr>
									<td><label>Activar</label></td>
									<td><label>Dato</label></td>
									<td><label>Valor mínimo</label></td>
									<td><label>Valor máximo</label></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="activarAgua"></input></td>
									<td><label>Nivel agua (%)</label></td>
									<td><input type="number" name="umbralAgua" min="1" max="100" required></td>
									<td>-</td>
								</tr>
								<tr>
									<td><input type="checkbox" name="activarTemp"></input> </td>
									<td><label>Temperatura (ºC)</label></td>
									<td><input type="number" name="umbralTempMin" min="1" max="100" required></td>
									<td><input type="number" name="umbralTempMax" min="1" max="100" required></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="activarHum"></input></td>
									<td><label> Humedad (%)</label></td>
									<td><input type="number" name="umbralHumMin" min="1" max="100" required></td>
									<td><input type="number" name="umbralHumMax" min="1" max="100" required></td>
								</tr>
							</table>
							<button class="btn btn-success" type="submit" >Guardar</button>
							<button class="btn btn-primary" type="button" onclick="alertasDefault()">Por defecto</button>
						</form>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
			
			<div class="box box-solid box-primary">
				<div class="box-header with-border">
				  <h3 class="box-title">Configuración personal</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<form action="gestores/guardarUser.php" method="POST">
							Aquí puedes cambiar la configuración personal del usuario.
							<br>
							<br>
							<table class="table table-bordered table-striped dataTable">
								<tr>
									<td><label>Nombre de usuario:</label></td>
									<td><input type="text" name="username" value="<?php echo $_SESSION['user']; ?>" required></td>
								</tr>
								<tr>
									<td><label>E-mail:</label></td>
									<td><input type="email" name="email" value="" required></td>
								</tr>
								<tr>
									<td><label>Contraseña actual:</label></td>
									<td><input type="password" name="passActual" required></td>
								</tr>
								<tr>
									<td><label>Nueva contraseña:</label></td>
									<td><input type="password" name="passNueva" required></td>
								</tr>
								<tr>
									<td><label>Repetir contraseña:</label></td>
									<td><input type="password" name="passNueva2" required></td>
								</tr>
							</table>
							<button class="btn btn-success" type="submit" >Guardar</button>
						</form>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-md-6">
			<div class="box box-solid box-primary">
				<div class="box-header with-border">
				  <h3 class="box-title">Actuadores</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<form action="gestores/guardarActuadores.php" method="POST">
							Configuración de los diferentes actuadores del sistema <b>PiFarmer</b>. 
							Aquí puedes definir cuándo y cuánto se deben activar los ventiladores y puedes definir 
							los días en que las luces y el riego se activarán.
							<br>
							<br>
							<label>
								Ventilación:<br>
							</label>
							<br>
							
							<table class="table table-bordered table-striped dataTable">
								<tr>
									<td><label>Activar</label></td>
									<td><label>Temperatura (ºC)</label></td>
									<td><label>Duración (min)</label></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="activarVentiladores"></input></td>
									<td><input type="number" name="umbralTempVent" min="1" max="99" required></td>
									<td><input type="number" name="duracionMin" min="1" max="59" required></td>
								</tr>
							</table>
							
							<label>
								Luces:
							</label>
							
							<table class="table table-bordered table-striped dataTable">
								<tr>
									<td><label>Activar</label></td>
									<td><label>Día</label></td>
									<td><label>Hora inicio</label></td>
									<td><label>Hora final</label></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="activarLucesLunes"></input></td>
									<td>Lunes</td>
									<td>de <?php generaHoras("lunesInicio"); ?></td>
									<td>a <?php generaHoras("lunesFinal"); ?></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="activarLucesMartes"></input></td>
									<td>Martes</td>
									<td>de <?php generaHoras("martesInicio"); ?></td>
									<td>a <?php generaHoras("martesFinal"); ?></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="activarLucesMiercoles"></input></td>
									<td>Miércoles</td>
									<td>de <?php generaHoras("miercolesInicio"); ?></td>
									<td>a <?php generaHoras("miercolesFinal"); ?></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="activarLucesJueves"></input></td>
									<td>Jueves</td>
									<td>de <?php generaHoras("juevesInicio"); ?></td>
									<td>a <?php generaHoras("juevesFinal"); ?></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="activarLucesViernes"></input></td>
									<td>Viernes</td>
									<td>de <?php generaHoras("viernesInicio"); ?></td>
									<td>a <?php generaHoras("viernesFinal"); ?></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="activarLucesSabado"></input></td>
									<td>Sábado</td>
									<td>de <?php generaHoras("sabadoInicio"); ?></td>
									<td>a <?php generaHoras("sabadoFinal"); ?></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="activarLucesDomingo"></input></td>
									<td>Domingo</td>
									<td>de <?php generaHoras("domingoInicio"); ?></td>
									<td>a <?php generaHoras("domingoFinal"); ?></td>
								</tr>
							</table>

							<label>
								Riego automático:
							</label>
							<br>
							
							<table class="table table-bordered table-striped dataTable">
								<tr>
									<td><label>Activar</label></td>
									<td><label>Día</label></td>
									<td><label>Hora</label></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="activarRiegoLunes"></input></td>
									<td>Lunes</td>
									<td>a las <?php generaHoras("lunesActiva"); ?></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="activarRiegoMartes"></input></td>
									<td>Martes</td>
									<td>a las <?php generaHoras("martesActiva"); ?></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="activarRiegoMiercoles"></input></td>
									<td>Miércoles</td>
									<td>a las <?php generaHoras("miercolesActiva"); ?></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="activarRiegoJueves"></input></td>
									<td>Jueves</td>
									<td>a las <?php generaHoras("juevesActiva"); ?></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="activarRiegoViernes"></input></td>
									<td>Viernes</td>
									<td>a las <?php generaHoras("viernesActiva"); ?></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="activarRiegoSabado"></input></td>
									<td>Sábado</td>
									<td>a las <?php generaHoras("sabadoActiva"); ?></td>
								</tr>
								<tr>
									<td><input type="checkbox" name="activarRiegoDomingo"></input></td>
									<td>Domingo</td>
									<td>a las <?php generaHoras("domingoActiva"); ?></td>
								</tr>
							</table>
							<label>Cm del depósito: </label>
							<input type="number" name="distanciaDep" min="2" max="400" required>
							<br><br>
							<button class="btn btn-success" type="submit" >Guardar</button>
							<button class="btn btn-primary" type="button" onclick="actuadoresDefault()">Por defecto</button>

						</form>
					</div>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->          
        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>AdminLTE Version</b> 2.3.3
    </div>
    <strong>Creado por Guillermo Figueras</a>.</strong> 
  </footer>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
<script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Script settings -->
<script src="dist/js/settings.js"></script>
<!-- Sweetalert js -->
<script src="plugins/sweetalert/sweetalert.min.js"></script>
</body>
</html>
<?php if(isset($_GET['hecho']) && $_GET['hecho']==1): ?>
			<script>
				swal("Correcto", "Cambios guardados correctamente", "success");
			</script>
<?php endif;
	  if(isset($_GET['hecho']) && $_GET['hecho']==0): ?>
			<script>
				swal("Error", "Los datos no fueron guardados correctamente", "error");
			</script>
<?php endif; ?>
