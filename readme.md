# PiFarmer #
PiFarmer és un projecte per a poder realitzar cultius automatizats fent servir una Raspberry Pi, encarregada de rebre i emmagatzemar les dades recollides per uns sensors.
Les principals funcionalitats del sistema PiFarmer són:

* Fotografia diària
* Recull de les dades captades pels sensors (Temperatura, Humitat, Nivell del diposit d'aigua)
* Visualització online amb la càmara web
* Programar les hores de llum
* Programar el rec automàtic
* Rebre alertes per e-mail 

## 1. Components a utilitzar: ##
* Raspberry Pi B+ amb sistema raspbian (~35€)
* Tarjeta MicroSD de 8 GB (~2€)
* Carregador micro USB 5V 2A (~8€)
* Funda Raspberry Pi (~6€)
* Sensor d'humitat i de temperatura DHT22 (~5€)
* Sensor ultrasons/proximitat HC-SR04 (~2€)
* Càmara web USB Eyetoy PS2
* Placa de 4 relés (~7€)
* Protoboard (~3€)
* Cables i resistències (~3€)
* Pels relés: les llums que vulguis, la electrovàlvula que vulguis i un ventilador de 5V

## 2. Instal·lació de la part física: ##
Seguir l'esquema del fitxer circuito_raspberry.png
A continuació cal posar la càmara a qualsevol dels ports USB.

## 3. Instal·lació del sistema: ##
### 3.1 Instal·lació de la arquitectura LAMP (php, apache2, mysql) i coses bàsiques ###
```bash
sudo apt-get update
sudo apt-get install apache2 mysql-server php5-mysql build-essential git-core python-dev python-openssl python-mysqldb
```
### 3.2 Clonació del repositori ###
Amb la següent comanda ens baixem els continguts del repositori (en aquest cas el repositori és privat):
```bash
git clone https://guillermo_a14_figueras_alba@bitbucket.org/guillermo_a14_figueras_alba/pifarmer.git
```

### 3.3 Instal·lació i configuració de la càmara ###
3.3.1 Comprovem que la càmara estigui connectada

Primer llistem els dispositius USB:
```bash
lsusb
```
Busquem l'ID i el substituim a la X:
```bash
lsusb -s1:X -v
```
3.3.2 Instal·lem V4L si no el tenim
```bash
sudo apt-get install v4l-utils
```
3.3.3 Confirmem que el dispositiu de vídeo estigui connectat
```bash
ls -al /dev/video*
```
3.3.4 Mostrem informació de la cámara:
```bash
sudo v4l2-compliance -d /dev/video0
```
3.3.5 Instal·lem motion (per fer streaming):
```bash
sudo apt-get install motion
```
3.3.6 Configuració de motion:

Obrim el primer fitxer de configuració:
```bash
sudo nano /etc/motion/motion.conf
```
Modifiquem els següents paràmetres:

* daemon OFF a ON
* webcam_localhost ON a OFF
* width 320 a 640
* height 240 a 480
* ffmpeg_video_codec swf a mpeg4

Obrim el següent fitxer de configuració:
```bash
sudo nano /etc/default/motion
```
Modifiquem el paràmetre:
* start_motion_daemon NO a YES
Reiniciem el servei de la càmara:
```bash
sudo service motion restart
```
3.3.7 Instal·lació de fswebcam (per treure fotos):
```bash
sudo apt-get install fswebcam
```

### 3.4 Instal·lació dels drivers necessaris per a llegir els sensors ###
Fem el següent git clone a on vulguem:
```bash
git clone https://github.com/adafruit/Adafruit_Python_DHT.git
```
Ens movem al directori descarregat i instal·lem els drivers:
```bash
cd Adafruit_Python_DHT
sudo python setup.py install
```

### 3.5 Configuració dels scripts i crontab (automatització) ###
Primerament hem de moure la carpeta scripts a la ruta /home/pi.
És a dir, que ha de quedar de la següent manera: /home/pi/scripts.
Un cop fet això, hem de configurar la connexió a la BBDD dels scripts.
Hem de canviar aquestes línies de scriptAlertasEmail.py, scriptLecturaSensores.py i scriptRele.py:
```python
mydb = MySQLdb.connect(host='localhost',
    user='root',
    passwd='root',
    db='pifarmer')
```

A continuació obrim el crontab amb permisos d'administrador:
```bash
sudo crontab -e
```
Afegim les línies següents:
```bash
# Hace una lectura de los valores de los sensores cada hora e inserta en la DB
@hourly /usr/bin/python /home/pi/scripts/scriptLecturaSensores.py
# Saca una foto cada dia a las 18:00
0 18 * * * /home/pi/scripts/scriptFoto.sh
# Envia emails a cada hora +1 min (10:01, 11:01, 12:01...)
1 * * * * /usr/bin/python /home/pi/scripts/scriptAlertasEmail.py
# Activa o desactiva los reles (luces, riego y ventilacion)
@hourly /usr/bin/python /home/pi/scripts/scriptRele.py
```


### 3.6 Per a enviar e-mails ###
Per a poder rebre les alertes de e-mails:
```bash
sudo apt-get install ssmtp mailutils
```
A scriptAlertasEmail.py hem de modificar la línia de la password de la nostra compta per a enviar els e-mails. L'usuari és el que el client hagi escollit (a settings.php):
```php
# Define SMTP email server details
	smtp_server = 'localhost'
	smtp_user   = email
	smtp_pass   = 'pass'
``` 
Es posible que es rebi un e-mail dient que una aplicació no segura está accedint al correu. Si pasa això, s'ha d'obrir l'enllaç de dit e-mail i canviar el paràmetre.

### 3.7 Posar en marxa el portal web ###
S'ha de moure la carpeta html a /var/www i comprovar que els permisos siguin els adequats.
Després s'ha de modificar el fitxer db_conn.php per a que els fitxers php puguin accedir a la BBDD
```php
	$servername = "localhost";
	$username = "root";
	$password = "pass";
	$db_name = "pifarmer";
```

### 3.8 Configuració SSL (https) ###
Seguir els pasos d'aquest tutorial:
https://raspberryando.wordpress.com/2014/05/03/activar-https-en-el-apache-de-nuestra-rpi/
També hem de canviar el DocumentRoot per a que accedim directament al directori /var/www/html, tant per l'SSL com per la connexió per http.
Haurem de modificar el contingut dels dos fitxers de configuració situats a /etc/apache2/sites-available/ i posar la ruta correcta:
```bash
<VirtualHost *:80>
    DocumentRoot /var/www/html
</VirtualHost>
```
