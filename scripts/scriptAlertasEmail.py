#!/usr/bin/env python
# -*- coding: utf-8 -*-

import MySQLdb
import smtplib
 
# Import the email modules
from email.mime.text import MIMEText

# Conexion a la BBDD
mydb = MySQLdb.connect(host='localhost',
    user='root',
    passwd='ausias',
    db='pifarmer')
cursor = mydb.cursor()

# 0 - activar, 1 - umbralMin, 2 - umbralMax
# Agua
cursor.execute("SELECT * FROM Alertas_email WHERE id=1")
results = cursor.fetchall()
for row in results:
	aguaActivar = row[2]
	aguaMin = row[3]
	
# Temperatura
cursor.execute("SELECT * FROM Alertas_email WHERE id=2")
results = cursor.fetchall()
for row in results:
	temperaturaActivar = row[2]
	temperaturaMin = row[3]
	temperaturaMax = row[4]
	
# Humedad
cursor.execute("SELECT * FROM Alertas_email WHERE id=3")
results = cursor.fetchall()
for row in results:
	humedadActivar = row[2]
	humedadMin = row[3]
	humedadMax = row[4]
	
# Ultima lectura Agua
cursor.execute("SELECT * FROM Nivell_aigua ORDER BY data DESC LIMIT 1")
results = cursor.fetchall()
for row in results:
	ultimaAgua = row[0]
	
# Ultima lectura Temperatura
cursor.execute("SELECT * FROM Temperatura ORDER BY data DESC LIMIT 1")
results = cursor.fetchall()
for row in results:
	ultimaTemp = row[0]
	
# Ultima lectura Humedad
cursor.execute("SELECT * FROM Humitat ORDER BY data DESC LIMIT 1")
results = cursor.fetchall()
for row in results:
	ultimaHum = row[0]


# Verificar si ya hay alguna alarma activada 
# Alarma nivel de agua minimo
cursor.execute("SELECT * FROM Alarmas_activadas WHERE id=1")
results = cursor.fetchall()
for row in results:
	alarmaAguaMin = row[2]
# Alarma temperatura minima
cursor.execute("SELECT * FROM Alarmas_activadas WHERE id=2")
results = cursor.fetchall()
for row in results:
	alarmaTempMin = row[2]
# Alarma temperatura maxima
cursor.execute("SELECT * FROM Alarmas_activadas WHERE id=3")
results = cursor.fetchall()
for row in results:
	alarmaTempMax = row[2]
# Alarma humedad minima
cursor.execute("SELECT * FROM Alarmas_activadas WHERE id=4")
results = cursor.fetchall()
for row in results:
	alarmaHumMin = row[2]
# Alarma humedad maxima
cursor.execute("SELECT * FROM Alarmas_activadas WHERE id=5")
results = cursor.fetchall()
for row in results:
	alarmaHumMax = row[2]

msg = "Se ha activado el servicio de alertas:\n"

activarEmail = False

if aguaActivar == 1:
	print "Las alertas de agua estan activadas"
	# Si la ultima lectura de agua pasa del umbral establecido y la alarma
	# no ha sido activada, añade al mensaje la alerta
	if ultimaAgua < aguaMin and not alarmaAguaMin:
		print "La ultima lectura de agua esta por debajo del umbral minimo y no se habia activado la alarma todavia"
		msg += "Nivel de agua por debajo del " + str(aguaMin) +"%."
		activarEmail = True
		cursor.execute("UPDATE Alarmas_activadas SET alarmaActivada=true WHERE id=1")

	# Vuelve a poner la alarma en desactivada si vuelve a estar por encima
	# del umbral deseado
	if alarmaAguaMin and ultimaAgua > aguaMin:
		print "La alarma ya habia sido activada y la ultima lectura de agua vuelve a estar por encima del umbral minimo"
		cursor.execute("UPDATE Alarmas_activadas SET alarmaActivada=false WHERE id=1")

if temperaturaActivar == 1:
	print "\nLas alertas de temperatura estan activadas"
	if ultimaTemp < temperaturaMin and not alarmaTempMin:
		print "La ultima lectura de temperatura esta por debajo del umbral minimo y no se habia activado la alarma todavia"
		msg += "Temperatura por debajo de " + str(temperaturaMin) +"ºC."
		activarEmail = True
		cursor.execute("UPDATE Alarmas_activadas SET alarmaActivada=true WHERE id=2")
	if ultimaTemp > temperaturaMax and not alarmaTempMax:
		print "La ultima lectura de temperatura esta por encima del umbral maximo y no se habia activado la alarma todavia"
		msg += "Temperatura por encima de " + str(temperaturaMax) +"ºC."
		activarEmail = True
		cursor.execute("UPDATE Alarmas_activadas SET alarmaActivada=true WHERE id=3")

	if alarmaTempMin and ultimaTemp > temperaturaMin:
		print "La alarma ya habia sido activada y la ultima lectura de temperatura vuelve a estar por encima del umbral minimo"
		cursor.execute("UPDATE Alarmas_activadas SET alarmaActivada=false WHERE id=2")
	if alarmaTempMax and ultimaTemp < temperaturaMax:
		print "La alarma ya habia sido activada y la ultima lectura de temperatura vuelve a estar por debajo del umbral maximo"
		cursor.execute("UPDATE Alarmas_activadas SET alarmaActivada=false WHERE id=3")

if humedadActivar == 1:
	print "\nLas alertas de humedad estan activadas"
	if ultimaHum < humedadMin and not alarmaHumMin:
		print "La ultima lectura de humedad esta por debajo del umbral minimo y no se habia activado la alarma todavia"
		msg += "Humedad por debajo del " + str(humedadMin) + "%."
		activarEmail = True
		cursor.execute("UPDATE Alarmas_activadas SET alarmaActivada=true WHERE id=4")
	if ultimaHum > humedadMax and not alarmaHumMax:
		print "La ultima lectura de humedad esta por encima del umbral maximo y no se habia activado la alarma todavia"
		msg += "Humedad por encima del " + str(humedadMax) + "%."
		activarEmail = True
		cursor.execute("UPDATE Alarmas_activadas SET alarmaActivada=true WHERE id=5")
		
	if alarmaHumMin and ultimaHum > humedadMin:
		print "La alarma ya habia sido activada y la ultima lectura de humedad vuelve a estar por encima del umbral minimo"
		cursor.execute("UPDATE Alarmas_activadas SET alarmaActivada=false WHERE id=4")
	if alarmaHumMax and ultimaHum < humedadMax:
		print "La alarma ya habia sido activada y la ultima lectura de humedad vuelve a estar por debajo del umbral maximo"
		cursor.execute("UPDATE Alarmas_activadas SET alarmaActivada=false WHERE id=5")

mydb.commit()
		
if activarEmail == True:
	cursor.execute("SELECT * FROM User")
	results = cursor.fetchall()
	for row in results:
		email = row[2]

	# Define SMTP email server details
	smtp_server = 'localhost'
	smtp_user   = email
	smtp_pass   = 'pass'

	# Construct email
	msg = MIMEText(msg)
	msg['To'] = email
	msg['From'] = email
	msg['Subject'] = 'Alerta PiFarmer'

	# Send the message via an SMTP server
	s = smtplib.SMTP('smtp.gmail.com', 587)
	s.ehlo()
	s.starttls()
	s.login(smtp_user,smtp_pass)
	s.sendmail(email, email, msg.as_string())
	s.quit()
