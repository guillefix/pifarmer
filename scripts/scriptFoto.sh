#!/bin/bash

day=$(date '+%Y-%m-%d')
service motion stop
fswebcam -d /dev/video0 -r 640x480 /var/www/html/fotos/$day.jpeg -S 2
service motion start
