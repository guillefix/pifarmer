#!/usr/bin/python

import MySQLdb
import subprocess
import RPi.GPIO as GPIO
import time
from decimal import Decimal
GPIO.setmode(GPIO.BCM)

TRIG = 20 
ECHO = 21

# Lectura de la distancia del deposito
print "Distance Measurement In Progress"

GPIO.setup(TRIG,GPIO.OUT)
GPIO.setup(ECHO,GPIO.IN)

GPIO.output(TRIG, False)
print "Waiting For Sensor To Settle"
time.sleep(2)

GPIO.output(TRIG, True)
time.sleep(0.00001)
GPIO.output(TRIG, False)

while GPIO.input(ECHO)==0:
  pulse_start = time.time()

while GPIO.input(ECHO)==1:
  pulse_end = time.time()

pulse_duration = pulse_end - pulse_start

distance = pulse_duration * 17150

distance = round(distance, 2)

print "Distancia:",distance,"cm"

GPIO.cleanup()


# Datos de humedad y de temperatura
subprocess.call("/home/pi/scripts/creaArchivoSensores.sh")
file = open('/home/pi/scripts/dataSensores.txt', 'r')
temp = file.readline()
file.close()
datos = temp.split("#")
print "Temperatura: ",datos[0],"C"
print "Humedad: ",datos[1],"%"


# Conexion a la BBDD
mydb = MySQLdb.connect(host='localhost',
    user='root',
    passwd='ausias',
    db='pifarmer')
cursor = mydb.cursor()


# Nivel del agua
cursor.execute("SELECT distancia FROM Deposito WHERE id=1")
results = cursor.fetchall()
for row in results:
	distanciaDB = row[0]
	# Muestra distancia bbdd
	print "distanciaDB=%s cm" % \
		(distanciaDB)

nivelAgua = (Decimal(distance)/Decimal(distanciaDB)) * 100
print "Nivel de agua:",nivelAgua,"%"


# Inserciones BBDD
cursor.execute("INSERT INTO Temperatura(valor, data) VALUES(%s, now())", datos[0])
cursor.execute("INSERT INTO Humitat(valor, data) VALUES(%s, now())", datos[1])
cursor.execute("INSERT INTO Nivell_aigua(valor, data) VALUES(%.1f, now())" % nivelAgua)


# Cierre
mydb.commit()	
cursor.close() 
