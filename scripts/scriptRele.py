#!/usr/bin/python  

import MySQLdb
import RPi.GPIO as GPIO  
import time  
import datetime 
from datetime import date
import calendar

GPIO.setmode(GPIO.BCM)
  
# Lista de reles: rele 1 = GPIO27 (luces), rele 2 = GPIO22 (riego), rele 3 = GPIO24 (ventilador)
pinList = [27, 22, 24]


# Conexion a la BBDD
mydb = MySQLdb.connect(host='localhost',
    user='root',
    passwd='ausias',
    db='pifarmer')
cursor = mydb.cursor()



# Ventilacion
cursor.execute("SELECT * FROM Ventilacion WHERE id=1")
results = cursor.fetchall()
for row in results:
	ventAct = row[1] 
	ventUmbral = row[2] 
	ventDuracion = row[3] 

# Luces
# Luces_lunes
cursor.execute("SELECT * FROM Dias_luces WHERE id=1")
results = cursor.fetchall()
for row in results:
	lucesLunAct = row[1] 
	lucesLunIni = row[3] 
	lucesLunFin = row[4] 
	
# Luces_martes
cursor.execute("SELECT * FROM Dias_luces WHERE id=2")
results = cursor.fetchall()
for row in results:
	lucesMarAct = row[1] 
	lucesMarIni = row[3] 
	lucesMarFin = row[4] 
	
# Luces_miercoles
cursor.execute("SELECT * FROM Dias_luces WHERE id=3")
results = cursor.fetchall()
for row in results:
	lucesMieAct = row[1] 
	lucesMieIni = row[3] 
	lucesMieFin = row[4] 
	
# Luces_jueves
cursor.execute("SELECT * FROM Dias_luces WHERE id=4")
results = cursor.fetchall()
for row in results:
	lucesJueAct = row[1] 
	lucesJueIni = row[3] 
	lucesJueFin = row[4] 
	
# Luces_viernes
cursor.execute("SELECT * FROM Dias_luces WHERE id=5")
results = cursor.fetchall()
for row in results:
	lucesVieAct = row[1] 
	lucesVieIni = row[3] 
	lucesVieFin = row[4] 
	
# Luces_sabado
cursor.execute("SELECT * FROM Dias_luces WHERE id=6")
results = cursor.fetchall()
for row in results:
	lucesSabAct = row[1] 
	lucesSabIni = row[3] 
	lucesSabFin = row[4] 
	
# Luces_domingo
cursor.execute("SELECT * FROM Dias_luces WHERE id=7")
results = cursor.fetchall()
for row in results:
	lucesDomAct = row[1] 
	lucesDomIni = row[3] 
	lucesDomFin = row[4] 
	

# RIEGO
# riego_lunes
cursor.execute("SELECT * FROM Dias_riego WHERE id=1")
results = cursor.fetchall()
for row in results:
	riegoLunAct = row[1] 
	riegoLunIni = row[3] 

	
# riego_martes
cursor.execute("SELECT * FROM Dias_riego WHERE id=2")
results = cursor.fetchall()
for row in results:
	riegoMarAct = row[1] 
	riegoMarIni = row[3] 
	
# riego_miercoles
cursor.execute("SELECT * FROM Dias_riego WHERE id=3")
results = cursor.fetchall()
for row in results:
	riegoMieAct = row[1] 
	riegoMieIni = row[3] 
	
# riego_jueves
cursor.execute("SELECT * FROM Dias_riego WHERE id=4")
results = cursor.fetchall()
for row in results:
	riegoJueAct = row[1] 
	riegoJueIni = row[3] 
	
# riego_viernes
cursor.execute("SELECT * FROM Dias_riego WHERE id=5")
results = cursor.fetchall()
for row in results:
	riegoVieAct = row[1] 
	riegoVieIni = row[3] 
	
# riego_sabado
cursor.execute("SELECT * FROM Dias_riego WHERE id=6")
results = cursor.fetchall()
for row in results:
	riegoSabAct = row[1] 
	riegoSabIni = row[3] 
	
# riego_domingo
cursor.execute("SELECT * FROM Dias_riego WHERE id=7")
results = cursor.fetchall()
for row in results:
	riegoDomAct = row[1] 
	riegoDomIni = row[3] 

# Ultima lectura Temperatura
cursor.execute("SELECT * FROM Temperatura ORDER BY data DESC LIMIT 1")
results = cursor.fetchall()
for row in results:
	ultimaTemp = row[0]


# loop through pins and set mode 
for i in pinList:
	GPIO.setup(i, GPIO.OUT)

now = datetime.datetime.now()

if now.hour <= 9:
	hora = "0"+str(now.hour)
else:
	hora = str(now.hour)

if now.minute <= 9:
	minuto = "0"+str(now.minute)
else:
	minuto = str(now.minute)

if now.second <= 9:
	segundo = "0"+str(now.second)
else:
	segundo = str(now.second)
	
	

nowStr = hora + ":" + minuto + ":" + segundo

my_date = date.today()
weekDay = calendar.day_name[my_date.weekday()]

print "\nFecha actual: " + nowStr
print "Dia semana: " + weekDay

# Luces lunes
if lucesLunAct and nowStr >= str(lucesLunIni) and nowStr < str(lucesLunFin) and weekDay == 'Monday':
	GPIO.output(27, GPIO.LOW)
# Luces martes
elif lucesMarAct and nowStr >= str(lucesMarIni) and nowStr < str(lucesMarFin) and weekDay == 'Tuesday':
	GPIO.output(27, GPIO.LOW)
# Luces miercoles
elif lucesMieAct and nowStr >= str(lucesMieIni) and nowStr < str(lucesMieFin) and weekDay == 'Wednesday':
	GPIO.output(27, GPIO.LOW)
# Luces jueves
elif lucesJueAct and nowStr >= str(lucesJueIni) and nowStr < str(lucesJueFin) and weekDay == 'Thursday':
	GPIO.output(27, GPIO.LOW)
# Luces Viernes
elif lucesVieAct and nowStr >= str(lucesVieIni) and nowStr < str(lucesVieFin) and weekDay == 'Friday':
	GPIO.output(27, GPIO.LOW)
# Luces sabado
elif lucesSabAct and nowStr >= str(lucesSabIni) and nowStr < str(lucesSabFin) and weekDay == 'Saturday':
	GPIO.output(27, GPIO.LOW)
# Luces domingo
elif lucesDomAct and nowStr >= str(lucesDomIni) and nowStr < str(lucesDomFin) and weekDay == 'Sunday':
	GPIO.output(27, GPIO.LOW)
else:
	GPIO.output(27, GPIO.HIGH)


# margen de 20 segundos 
nowStrMargen = str(now.hour) + ":" + str(now.minute) + ":" + str(now.second + 20)

# riego lunes
if riegoLunAct and nowStr >= str(riegoLunIni) and nowStr <= nowStrMargen and weekDay == 'Monday':
	GPIO.output(22, GPIO.LOW)
# riego martes
elif riegoMarAct and nowStr >= str(riegoMarIni) and nowStr <= nowStrMargen and weekDay == 'Tuesday':
	GPIO.output(22, GPIO.LOW)
# riego miercoles
elif riegoMieAct and nowStr >= str(riegoMieIni) and nowStr <= nowStrMargen and weekDay == 'Wednesday':
	GPIO.output(22, GPIO.LOW)
# riego jueves
elif riegoJueAct and nowStr >= str(riegoJueIni) and nowStr <= nowStrMargen and weekDay == 'Thursday':
	GPIO.output(22, GPIO.LOW)
# riego Viernes
elif riegoVieAct and nowStr >= str(riegoVieIni) and nowStr <= nowStrMargen and weekDay == 'Friday':
	GPIO.output(22, GPIO.LOW)
# riego sabado
elif riegoSabAct and nowStr >= str(riegoSabIni) and nowStr <= nowStrMargen and weekDay == 'Saturday':
	GPIO.output(22, GPIO.LOW)
# riego domingo
elif riegoDomAct and nowStr >= str(riegoDomIni) and nowStr <= nowStrMargen and weekDay == 'Sunday':
	GPIO.output(22, GPIO.LOW)
else:
	GPIO.output(22, GPIO.HIGH)
	

# Activar ventilacion
if ventAct and ultimaTemp > ventUmbral:
	GPIO.output(24, GPIO.LOW)
	time.sleep(ventDuracion * 60)
	GPIO.output(24, GPIO.HIGH)
else:
	GPIO.output(24, GPIO.HIGH)
